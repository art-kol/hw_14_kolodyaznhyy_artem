package schoolChat;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

public class Server {

    public static final int PORT = 8080;
    public static LinkedList<StudentConnection> serverList = new LinkedList<>();

    public static void main(String[] args) throws IOException {
        /**
         * Данные пользователей
         */
        List<List<String>> dataTest = new ArrayList<>();
        dataTest.add(Arrays.asList("John","12345"));
        dataTest.add(Arrays.asList("Mike", "54321"));
        dataTest.add(Arrays.asList("Mary", "qwerty"));

        BufferedReader in;
        BufferedWriter out;

        try (ServerSocket server = new ServerSocket(PORT)) {
            System.out.println("Школьный чат запущен");

            while (true) {

                Socket socket = server.accept();
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                out.write("Введите логин пароль\n");
                out.flush();

                /**
                 * Аутентификация
                 */
                String student = "";
                while (true) {
                    boolean isAccess = false;
                    String logIn = in.readLine();
                    String[] loginAndPass = logIn.split(" ");
                    for (List<String> l : dataTest) {
                        if (l.get(0).equals(loginAndPass[0]) && l.get(1).equals(loginAndPass[1])) {
                            student = l.get(0);
                            isAccess = true;
                            break;
                        }
                    }
                    if (!isAccess) {
                        out.write("Неверные данные, попробойте заного" + "\n");
                        out.flush();
                    } else {
                        out.write("Вы вошли как " + loginAndPass[0] + "\n");
                        out.flush();
                        break;
                    }
                }

                /**
                 * Добавить новое соеденение в список
                 */
                serverList.add(new StudentConnection(socket, student));
            }
        }
    }
}
