package schoolChat;

import java.io.*;
import java.net.Socket;

public class Student_1 {

    private Socket student;
    private BufferedWriter writer;
    private BufferedReader reader;
    private BufferedReader inputStudent;

    public Student_1(){
        try {
            student = new Socket("127.0.0.1", 8080);
            writer = new BufferedWriter(new OutputStreamWriter(student.getOutputStream()));
            reader = new BufferedReader(new InputStreamReader(student.getInputStream()));
            inputStudent = new BufferedReader(new InputStreamReader(System.in));

            Thread writeThread = new Thread(new WriteMsg());
            writeThread.start();

            Thread readThread = new Thread(new ReadMsg());
            readThread.start();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Класс для вывода сообщение в потоке
     */
    private class ReadMsg extends Thread {
        @Override
        public void run() {
            String str;
            try {
                while (true) {
                    str = reader.readLine();
                    System.out.println(str);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Класс для ввода сообщение в потоке
     */
    class WriteMsg implements Runnable {

        @Override
        public void run() {

            while (true) {
                String userWord;
                try {
                    userWord = inputStudent.readLine();
                    writer.write(userWord + "\n");
                    writer.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        new Student_1();
    }
}