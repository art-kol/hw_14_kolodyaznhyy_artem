package schoolChat;

import java.io.*;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

class StudentConnection extends Thread {

    private Socket socket;
    private BufferedReader in;
    private BufferedWriter out;
    private String student;
    private Date time;
    private String dtime;
    private SimpleDateFormat dt1;

    public StudentConnection(Socket socket, String student) throws IOException {
        this.socket = socket;
        this.student = student;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        start();
    }

    @Override
    public void run() {
        String word;
        try {
            /**
             * Если подключился новый пользователь все участники чата получают нотификацию кроме нового участинка
             */
            for (StudentConnection vr : Server.serverList) {
                if(!vr.student.equals(student)){
                    vr.send(student+" В сети ");
                }
            }
            while (true) {
                word = in.readLine();

                // Текущее время для отправки сообщения
                time = new Date();
                dt1 = new SimpleDateFormat("HH:mm:ss");
                dtime = dt1.format(time);

                /**
                 * Массив строк ввода с консоли
                 * limit 3 делит строку на 3 части: 1 команда, 2 разделение между командой и сообщение
                 * 3 само сообщение
                 */
                String[] sendingForm = word.split(" ", 3);

                if(!sendingForm[1].equals(">>")){
                    out.write("Неверная команда" + "\n");
                    out.flush();
                }

                /**
                 * Отправка сообщения всем пользователям
                 */
                if(sendingForm[0].equals("all")){
                    for (StudentConnection vr : Server.serverList) {
                        vr.send("(" + dtime + ") "+"Сообщение всем от "+student+": "+sendingForm[2]+"\n");
                    }
                } else {
                /**
                 * Отправка сообщения конкретному пользователю
                 */
                    for (StudentConnection vr : Server.serverList) {
                        if(vr.getStudent().equals(sendingForm[0])){
                            vr.send("(" + dtime + ") "+"Сообщение от "+student+": "+sendingForm[2]+"\n");
                            this.send("(" + dtime + ") "+"Отправлено "+sendingForm[0]+": "+sendingForm[2]+"\n");
                        }
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод для отправки сообщения
     * @param msg - сообщение для отправки
     */
    private void send(String msg) {
        try {
            out.write(msg + "\n");
            out.flush();
        } catch (IOException ignored) {}
    }

    public String getStudent() {
        return student;
    }
}